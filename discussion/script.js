// Comparison Query Operators
//  $gt/$gte operator
	db.collectionName.find({ field: { $gt : value } });
	db.collectionName.find({ field: { $gte : value } });

	db.users.find({ age: { $gt: 50 } });

// $lt/$lte operator
	db.users.find({ age: { $lt: 50 } }).pretty();
	db.users.find({ age: { $lte: 50 } }).pretty();
// .pretty doesn't work on Robo 3T but it works on terminal
// changes it from one line to multiple lines that looks nicer and easier to read

// $ne operator
db.users.find({ age: { $ne: 82 } }).pretty();

// $in
db.users.find({ lastName: { $in: ["Hawking", "Doe"]}}).pretty();
db.users.find({ courses: { $in: ["HTML", "React"]}}).pretty();

// Logical Query Operators
// $or operator
db.users.find({ $or: [{firstName: "Neil"}, {age: "25"}]}).pretty();

// $and operator
db.users.find({ $and: [{age: {$ne: 82}}, {age: {$ne: 76}}]}).pretty();

// Field Projection
// Retrieving documents are common operations that we do and by default MongoDB queries return the whole document as a response.

// When dealing with complex data structures, there might be instances when fields are not useful for the query that we are trying to accomplish.

// To help with readability of the values returned, we can include/exclude fields from the response.

/*
	Inclusion
		- allows us to include / add specific fields only when retrieving documents
		- The value provided is 1 to denote that the field is being included
		- Syntax
			db.users.find({criteria}, {field: 1})
*/

db.users.find(
	{
		firstName: "Jane"
	},
	{
		contact: 0,
		department: 0
	}
).pretty();



/*
	Exclusion
		-allows us to excllude/remove specific fields only when retrieving documents
		- when using field projection, field inclusion and exclusion may not be used at the same time
		- excluding the "_id" field is the only exception to this rule
		- Syntax:
			db.users.find({criteria}. {_id: 0})
*/

db.users.find(
	{
		firstName: "Jane"
	},
	{
		firstName: 1,
		lastName: 1,
		contact: 1,
		_id: 0
	}
).pretty();

// Embeded
{
	id: 12345,
	title: "Titantic",
	genre: "Romanc",
	reviews: [
		{
			name: "Jherson",
			rating: "5",
			comments: "Lorem Ipsum"
		},
		{
			name: "Ethan",
			rating: "5",
			comments: "Lorem Ipsum"
		}
	]
}

// Another example of Embedded
{
	name: "Hannah",
	courses: [
		{
			course_name: "HTML"
		},
		{
			course_name: "PHP"
		}
	]
}

// Referenced - better option
{
	id: 12345,
	title: "Titantic",
	genre: "Romanc",
	reviews: [
		{
			reviewer_id: 12458-3352
		},
		{
			reviewer_id: 155658-3352
		}
	]
}


// Evaluation Query Operators

/*
	$regex operator
		- allows us to find documents that match a specific string pattern using regular expressions.
		- Syntax
			db.users.find({ field: $regex: 'pattern', $options: '$optionValue'})
*/

// Case sensitive query
db.users.find({firstName: {$regex: 'N'}});

// Case insensitive query
db.userse.find({firstName: { $regex: 'j', $options: '$i'}})


Users | Courses

{
	name: "Hannah",
	courses: [
		{
			courseName: "HTML"
		},
		{
			courseName: "PHP"
		}
	]
}

HTML
{
	name: "HTML"
	enrollees: ["Hannah", "Alan", "Lorenzo"]
}

