/*
  Activity
  
  1) Find users with letter 's' in their first name or 'd' in their last name.
		Show only the firstName and lastName fields and hide the _id field.
    
  2) Find users who are from the HR department 
  and their age is greater then or equal to 70.
  
  3) Find users with the letter 'e' in their first name 
  and has an age of less than or equal to 30.
*/

// For Number 1
db.users.find({ $or: [{firstName: {$regex: 's', $options: '$i'}}, {lastName: {$regex: 'd', $options: '$i'}}]}).pretty();

// For Number 2
db.users.find({ $and: [{department: {$in: ["HR"]}}, {age: {$gte: 70}}]}).pretty();

// For Number 3
db.users.find({ $and: [{firstName: {$regex: 'e', $options: '$i'}}, {age: {$lte: 30}}]}).pretty();

// Can't write down my answers because my collection has different names and ages